// compiler.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <string.h>
#include<stdio.h>
#include<stdlib.h>
struct blocktable
{
	char blockname[6];
	int jumpto;
};
struct symboltable
{
	char varname[3];
	int address ;
	int bytesize;
};
struct ic
{
	int op;
	int operands[5];
};
struct constants
{
	char var[3];
	int value;
};
char* read_input(char *filename)
{//preperation for filesystem
	char *buffer ;
	FILE * f = fopen(filename, "rb");
	int length;
	if (f)
	{
		fseek(f, 0, SEEK_END);
		length = ftell(f);
		fseek(f, 0, SEEK_SET);
		buffer = (char*)malloc(length + 2); 
		if (buffer)
		{
			fread(buffer, 1, length, f);
		}
		fclose(f);
		buffer[length++] = '\n';
		buffer[length] = '\0';
	}
	return buffer;
}
char** tokenize(char *line)
{
	char **tokens = (char**)malloc(sizeof(char*)*6);
	for (int i = 0; i < 6;i++)
	{
		tokens[i] = (char*)malloc(6);
	}
	int spaceremover = 0;
	for (spaceremover = 0; line[spaceremover] == ' '; spaceremover++);
	int n = 0, j = 0,flag=1;
	for (int i = spaceremover; line[i] != '\0';i++)
	{
		if ((line[i] == ' '||line[i]==',')&&flag)
		{
			tokens[n][j] = '\0';
			n++; j = 0;
			flag = 0;
		}
		else if (line[i] != ' '){ tokens[n][j++] = line[i]; flag = 1; }
		
	}
	tokens[n][j] = '\0';
	return tokens;
}
int searchsymbol(struct symboltable *variables, char* variablename)
{
	for (int i = 0;; i++)
	{
		if (strcmp(variables[i].varname, variablename) == 0)
			return variables[i].address;
	}
}
int getint(char* input)
{
	int len = strlen(input);
	int i;
	input[len] = '\0';
	for (i = 0; input[i] != '\0';i++)
	{
		input[i] = input[i + 1];
	}
	return atoi(input);
}
int getcomparator(char*op)
{
	if (strcmp(op, "EQ") == 0)
		return 8;

	if (strcmp(op, "LT") == 0)
		return 9;
	if (strcmp(op, "GT") == 0)
		return 10;

	if (strcmp(op, "LTEQ") == 0)
		return 11;

	if (strcmp(op, "GTEQ") == 0)
		return 12;

}
int getRegno( char *reg)
{
	if (strcmp(reg, "AX") == 0)
		return 0;

	if (strcmp(reg, "BX") == 0)
		return 1;
	if (strcmp(reg, "CX") == 0)
		return 2;

	if (strcmp(reg, "DX") == 0)
		return 3;

	if (strcmp(reg, "EX") == 0)
		return 4;

	if (strcmp(reg, "FX") == 0)
		return 5;

	if (strcmp(reg, "GX") == 0)
		return 6;

	if (strcmp(reg, "HX") == 0)
		return 7;
}
void processread(char**tokens, ic*lines, int*no_of_lines)
{
	lines[*no_of_lines].op = 14;
	lines[*no_of_lines].operands[0] = getRegno(tokens[1]);
	*no_of_lines += 1;
}
void processmove(char**tokens, ic*lines, int*no_of_lines, symboltable *variables)
{
	char *check = strchr(tokens[1], 'X');
	if (check)
	{
		lines[*no_of_lines].op = 2;
		lines[*no_of_lines].operands[0] = getRegno(tokens[1]);
		lines[*no_of_lines].operands[1] = searchsymbol(variables, tokens[2]);
	}
	else
	{
		lines[*no_of_lines].op = 1;
		lines[*no_of_lines].operands[1] = getRegno(tokens[2]);
		lines[*no_of_lines].operands[0] = searchsymbol(variables, tokens[1]);
	}
	*no_of_lines += 1;
}
void set_symbol( symboltable * variables,int address,int size,char* data,int num_of_variables )
{
	strcpy(variables[num_of_variables].varname, data);
	variables[num_of_variables].bytesize = 1;
	variables[num_of_variables].address = address;
}
int buildsymboltable(char **tokens, struct symboltable *variables, int *address, int* num_of_variables, constants*con,int*no_of_con)
{
	if (strcmp(tokens[0], "START:") == 0)
		return 0;
	if (strcmp(tokens[0], "DATA") == 0)
	{
		char* check = strchr(tokens[1], '[');
		//single decleration
		if (check == NULL)
		{

			strcpy(variables[*num_of_variables].varname, tokens[1]);
			variables[*num_of_variables].bytesize = 1;
			variables[*num_of_variables].address = *address;
			*address+=1;
			*num_of_variables+=1;
		}
		//array decleration
		else{
			int size = getint(check);
			check[0] = '\0';
			strcpy(variables[*num_of_variables].varname, tokens[1]);
			variables[*num_of_variables].bytesize = size;
			variables[*num_of_variables].address = *address;
			*address += size;
			*num_of_variables+=1;

		}
	}
	else
	{
		strcpy(variables[*num_of_variables].varname, tokens[1]);
		variables[*num_of_variables].bytesize = 1;
		variables[*num_of_variables].address = *address;
		con[*no_of_con].value = getint(tokens[3]);
		*address += 1;
		*num_of_variables+=1;
		*no_of_con += 1;
	}
	return 1;
}
void processarithmetic(char**tokens,ic* lines,int* no_of_lines)
{
	if (strcmp(tokens[0], "ADD") == 1)
	{
		lines[*no_of_lines].op = 3;
	}
	else
		lines[*no_of_lines].op = 4;
	lines[*no_of_lines].operands[0] = getRegno(tokens[1]);
	lines[*no_of_lines].operands[1] = getRegno(tokens[2]);
	lines[*no_of_lines].operands[2] = getRegno(tokens[3]);
	*no_of_lines+=1;
}
void processprint(char**tokens, ic* lines, int* no_of_lines,struct symboltable* variables)
{
	lines[*no_of_lines].op = 13;
	lines[*no_of_lines].operands[0] = searchsymbol(variables, tokens[1]);
	*no_of_lines += 1;
}
void processIF(char **tokens, ic *lines, int*no_of_lines, blocktable*blocks, int*linepointer, int*stack, int*top)
{
	lines[*no_of_lines].op = 7;
	lines[*no_of_lines].operands[0] = getRegno(tokens[1]);
	lines[*no_of_lines].operands[1] = getcomparator(tokens[2]);
	lines[*no_of_lines].operands[2] = getRegno(tokens[3]);
	lines[*no_of_lines].operands[3] = -1;
	stack[*top] = *linepointer;
	*top += 1;
	*no_of_lines += 1;
}
void processELSE(char**tokens, ic* lines, int* no_of_lines, blocktable* blocks,int*no_of_blocks, int* linepointer, int* stack, int* top)
{
	lines[*no_of_lines].op = 6;
	lines[*no_of_lines].operands[0] = -1;
	
	stack[*top] = *linepointer;
	*top+=1;
	strcpy(blocks[*no_of_blocks].blockname, "ELSE");
	blocks[*no_of_blocks].jumpto = *linepointer;
	*no_of_blocks += 1;
	*no_of_lines += 1;
}
void processENDIF(char**tokens,ic* lines,int* no_of_lines,blocktable* blocks,int*no_of_blocks,int* linepointer,int* stack,int* top)
{
	strcpy(blocks[*no_of_blocks].blockname, "ENDIF");
	blocks[*no_of_blocks].jumpto = *linepointer;
	*no_of_blocks += 1;
	int identify = 0;
	int pop = stack[*top-1]-1;
	*top -= 1;
	identify =lines[pop].op ;
	if (identify == 6)
	{
		int x = pop;
		lines[pop].operands[0] = *linepointer;
		pop = stack[*top - 1] - 1;
		*top -= 1;
		identify = lines[pop].op;
		lines[pop].operands[3] = x+2;
	}
	else
	{
		lines[pop].operands[3] = *linepointer;
	}

}
void processlabels(char**tokens,blocktable* blocks,int* no_of_blocks,int*linepointer)
{
	strcpy(blocks[*no_of_blocks].blockname , tokens[0]);
	blocks[*no_of_blocks].jumpto = *linepointer;
}
void processJUMP(char**tokens,ic* lines,int* no_of_lines,blocktable*blocks)
{
	lines[*no_of_lines].op = 6;
	for (int i = 0;;i++)
	{
		if (strcmp(blocks[i].blockname, tokens[1]) == 0)
		{
			lines[*no_of_lines].operands[0] = blocks[i].jumpto;
			break;
		}
	}
	*no_of_lines += 1;
}
int process(char **tokens,symboltable *variables,blocktable*blocks,int *num_of_variables,ic*lines,int* no_of_lines,int *linepointer,int*no_of_blocks,int*stack,int *top)
{
	if (strcmp(tokens[0], "READ") == 0)
	{
		processread(tokens, lines, no_of_lines);
	}
	else if (strcmp(tokens[0], "MOV") == 0)
	{
		processmove(tokens, lines, no_of_lines,variables);
		
	}
	else if (strcmp(tokens[0], "ADD") == 0 || strcmp(tokens[0], "SUB") == 0)
	{
		processarithmetic(tokens, lines, no_of_lines);
	}
	else if (strcmp(tokens[0], "PRINT") == 0)
	{
		processprint(tokens, lines, no_of_lines, variables);
	
	}
	else if (strcmp(tokens[0], "ELSE") == 0)
	{
		processELSE(tokens, lines, no_of_lines, blocks,no_of_blocks, linepointer, stack, top);
		
	}

	else if (strcmp(tokens[0], "ENDIF") == 0)
	{
		processENDIF(tokens, lines, no_of_lines, blocks,no_of_blocks, linepointer, stack, top);

	}
	else if (strcmp(tokens[0], "IF") == 0)
	{
		processIF(tokens, lines, no_of_lines, blocks,linepointer,stack,top);

	}
	else if (strcmp(tokens[0], "JUMP") == 0)
	{
		processJUMP(tokens, lines, no_of_lines,blocks);

	}

	else if (strcmp(tokens[0], "END") == 0)
	return 0;
	else
	{
		processlabels(tokens, blocks, no_of_blocks,linepointer);
	}
}
void printtofile(symboltable* variables,ic* lines,constants* con,blocktable*blocks,int num_of_variables,int num_of_cons,int no_of_lines,int no_of_blocks)
{
	FILE *fp = fopen("test.txt", "wb");
	fwrite(&num_of_variables, 4, 1, fp);
	for (int i = 0; i < num_of_variables;i++)
	{
		fwrite(&variables[i], sizeof(symboltable), 1, fp);
	}
	fwrite(&num_of_cons, 4, 1, fp);
	for (int i = 0; i < num_of_cons;i++)
	{
		fwrite(&con[i], sizeof(constants), 1, fp);
	}
	fwrite(&no_of_blocks, 4, 1, fp);
	for (int i = 0; i < no_of_blocks; i++)
	{
		fwrite(&blocks[i], sizeof(blocktable), 1, fp);
	}
	fwrite(&no_of_lines, 4, 1, fp);
	for (int i = 0; i < no_of_lines; i++)
	{
		fwrite(&lines[i], sizeof(ic), 1, fp);
	}
}
void getlines(char *buffer, struct symboltable *variables, struct ic*lines, struct constants*con, blocktable *blocks, int *address, int* num_of_variables, int* num_of_cons, int*no_of_lines, int*no_of_blocks, int*linepointer, int*stack, int*top)
{
	int i = 0, j = 0,flag=1;
	char line[100];
	char **tokens;
	while (flag)
	{
		j = 0;
		while (buffer[i] != '\r'&&buffer[i]!='\n')
		{
			line[j++] = buffer[i++];
			if (buffer[i] == '\r')
				i++;
		}

		line[j] = '\0';
		tokens = tokenize(line);
		flag=buildsymboltable(tokens,variables,  address,  num_of_variables,con,num_of_cons);
		i += 1;
	}
	flag = 1;
	while (flag)
	{
		*linepointer += 1;
		j = 0;
		while (buffer[i] != '\r'&&buffer[i] != '\n')
		{
			line[j++] = buffer[i++];
			if (buffer[i] == '\r')
				i++;
		}
		line[j] = '\0';
		tokens = tokenize(line);
		flag = process(tokens, variables,blocks, num_of_variables,lines,no_of_lines,linepointer,no_of_blocks,stack,top);
		i += 1;
	}
	printtofile(variables, lines, con,blocks, *num_of_variables, *num_of_cons, *no_of_lines,*no_of_blocks);
}
int main()
{
	int num_of_variables = 0;
	int num_of_cons = 0;
	int num_of_lines = 0;
	int num_of_blocks = 0;
	int linepointer = 0;
	int address = 0;
	int stack[30];
	int top = 0;
	symboltable* variables = (symboltable*)malloc(sizeof(symboltable) * 20);
	ic*lines = (ic*)malloc(sizeof(ic) * 100);
	blocktable*blocks = (blocktable*)malloc(sizeof(blocktable) * 20);
	constants*con = (constants*)malloc(sizeof(constants) * 20);
	char* buffer;
	buffer = read_input("input.txt");
	getlines(buffer,variables,lines,con,blocks ,&address, & num_of_variables,&num_of_cons,&num_of_lines,&num_of_blocks,&linepointer,stack,&top);
	return 0;
}

