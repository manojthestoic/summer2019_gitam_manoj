struct cell
{
	int flag;
	int value;
	char expression[30];
};
char* delimit(char*exp);
int isoperator(char*x);
char*postfix(char*exp);

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int toint(char s);
int isexpression(char *value);
char* print(char* str);
int getint(char* str);
float getinternal(char* opn, cell sheet[10][10], int * chararray, int * intarray, int  pointer);

float eval(char* exp, cell sheet[10][10], int * chararray, int * intarray, int  pointer);
char* delimit(char*exp);