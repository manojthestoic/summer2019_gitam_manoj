#include"Header.h"

int isalphadigit(char ch)
{
	if ((ch >= '0'&&ch <= '9') || (ch >= 'A'&&ch <= 'Z'))
		return 1;
	else return 0;
}

float eval(char* exp, cell sheet[10][10], int * chararray, int * intarray, int  pointer)
{
	char stack[30][6];
	int top = 0;
	int x = 0;
	while (exp[x] != '\0')
	{
		int i = 0;
		while (exp[x] != ' ')
		{
			if (isalphadigit(exp[x]))
			{
				while (exp[x] != ' ')
					stack[top][i++] = exp[x++];
				stack[top][i] = '\0';
				top++;
			}
			else
			{
				float res;
				switch (exp[x])
				{
				case '+':res = getinternal(stack[top - 2], sheet, chararray, intarray, pointer) + getinternal(stack[top - 1], sheet, chararray, intarray, pointer);
					break;
				case '-':res = getinternal(stack[top - 2], sheet, chararray, intarray, pointer) - getinternal(stack[top - 1], sheet, chararray, intarray, pointer);
					break;
				case '*':res = getinternal(stack[top - 2], sheet, chararray, intarray, pointer) * getinternal(stack[top - 1], sheet, chararray, intarray, pointer);
					break;
				case '/':res = (float)getinternal(stack[top - 2], sheet, chararray, intarray, pointer) / getinternal(stack[top - 1], sheet, chararray, intarray, pointer);
					break;
				}
				top = top - 2;
					gcvt(res,10, stack[top]);
					x++;
					top++;
				}
			}x++;

		}
		return atof(stack[0]);
	}
	char* delimit(char*exp)
	{
		int x = 0;
		int respointer = 0;
		char *result = (char*)malloc(sizeof(char) * 100);
		while (exp[x] != '\0'){
			if (isalphadigit(exp[x]))
			{
				while (isalphadigit(exp[x]))
				result[respointer++] = exp[x++];
				result[respointer++] = ' ';
			}
			else{
				result[respointer++] = exp[x++];
				result[respointer++] = ' ';
			}
		}
		result[--respointer] = '\0';
		return result;

	}
	int Prec(char ch)
	{
		switch (ch)
		{
		case '+':
		case '-':
			return 1;

		case '*':
		case '/':
			return 2;

		case '^':
			return 3;
		}
		return -1;
	}
	char*postfix(char*exp)
	{
		char stack[100];
		char temp[5];
		char *result = (char *)malloc(sizeof(char) * 100);
		int top = -1;
		int respointer = 0;
		int x = 0;
		while (exp[x] != '\0')
		{
			if (exp[x] == ' ')
				x++;
			int i = 0;
			while (exp[x] != ' '&&exp[x] != '\0')
			{
				temp[i] = exp[x];
				i++; x++;
			}
			temp[i] = '\0';
			if (isoperator(temp))
			{
				if (temp[0] == '(')
					stack[++top] = exp[x++];

				else if (temp[0] == ')')
				{
					while (top >= 0 && stack[top] != '(')
					{
						result[respointer++] = stack[top--];
						result[respointer++] = ' ';
					}
				}
				else // an operator is encountered 
				{
					while (top >= 0 && Prec(temp[0]) <= Prec(stack[top]))
					{
						result[respointer++] = stack[top--];
						result[respointer++] = ' ';
					}
					stack[++top] = temp[0];
				}
			}
			else
			{
				int cpy = 0;
				while (temp[cpy] != '\0')
				{
					result[respointer++] = temp[cpy++];

				}result[respointer++] = ' ';
			}

		}
		while (top >= 0)
		{
			result[respointer++] = stack[top--];
			result[respointer++] = ' ';
		}
		result[respointer] = '\0';
		return result;
	}