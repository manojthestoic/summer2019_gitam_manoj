// day3.cpp : Defines the entry point for the console application.
//

#include"Header.h"
#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

char*getfilename(char*location,int hflag)
{
	int i = 0;
	for ( i = 0; location[i] != '\0'; i++)
	{
		if (location[i] == '.')
			if (location[i+1]=='c')
				if (location[i + 2] == 's')
					if (location[i + 3] == 'v')
			{
				while (location[i] != '\0')
				{
					if (location[i] == '=')
						hflag = 1;
				}

				return location;
			}
	}
	i = 0;
	while (location[i++] != '\0')
	{
		if (location[i] == '=')
			hflag = 1;
	}
	char filename[20];
	int j = 0;
	while (location[j++] != ' ');
	location[--j] = '\0';

	strcpy(filename, location);
	strcat(filename, ".csv");
	
	return filename;
}


char* Toupper(char* string)
{
	for (int i = 0; string[i] != '\0'; i++)
	{
		string[i] = towupper(string[i]);
	}
	return string;
}


int main()
{
	char  headers[20][30];
	struct cell sheet[10][10];
	char ch;
	char filename[20];
	int isopen = 0;
	char input[300];
	char command[10];
	char location[300];
	char value[300];
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			sheet[i][j].value = 0;
			sheet[i][j].flag = 0;
		}
	}

	while (true)
	{
		
		printf(">");
		int i = 0, j = 0;
		gets(input);
		for (i = 0; input[i] != ' '&&input[i] != '\0'; i++)
		{
			command[i] = input[i];
		}
		command[i] = '\0';

		for (i; input[i] == ' '; i++);
		for (j = 0; input[i] != '\0'; i++)
			location[j++] = input[i];
		location[j] = '\0';

		for (int i = 0; command[i] != '\0'; i++)
		{
			command[i] = towupper(command[i]);
		}

		if (strcmp(command, "IMPORT") == 0)
		{
			FILE *fp;
			isopen = 1;
			int  hflag = (int )malloc(sizeof(int));
			hflag = 0;
			strcpy(filename, getfilename(location,hflag));
			fp = fopen(filename, "r");
			char readbuffer[30];
			for (int i = 0; i < 10; i++)
			{
				for (int j = 0; j < 10; j++)
				{
					int x = 0;
					while ((ch = fgetc(fp)) != ','&& ch != EOF)
					{
						if (ch == '\n')
						{
							x++; continue;
						}
						readbuffer[x++] = ch;
					}
					readbuffer[x] = '\0';
					if (isexpression(readbuffer))
					{
						sheet[i][j].flag = 1;
						strcpy(sheet[i][j].expression, readbuffer);
					}
					else
						sheet[i][j].value = getint(readbuffer);
				}
			}
		}

		//get


		else if (strcmp(command, "GET") == 0){
			//printf("%d", eval(postfix(delimit("A1+A2*A3")), sheet));
			for (int i = 0; location[i] != '\0'; i++)
			{
				location[i] = towupper(location[i]);
			}
			int* intarray = (int *)malloc(sizeof(int) * 100);
			int* chararray = (int *)malloc(sizeof(int) * 100);
			int pointer = (int)malloc(sizeof(int));
			pointer = 0;
			float result = getinternal(location, sheet, chararray, intarray, pointer);
			printf("%f\n", result);
		}

		else if (strcmp(command, "SET") == 0){
			for (int i = 0; location[i] != '\0'; i++)
			{
				location[i] = towupper(location[i]);
			}
			char temp[5];
			int lp = 0, tp;
			for (; location[lp] >= 'A'&&location[lp] <= 'Z'; lp++)
				temp[lp] = location[lp];
			int x = toint(location[0]);
			for (tp = 0; location[lp] >= '0'&&location[lp] <= '9'; lp++)
				temp[tp++] = location[lp];
			temp[tp] = '\0';
			int y = getint(temp) - 1;
			int recordpointer = 0;
			int k = 0;
			int vpointer = 0;
			for (k; location[k] != '='; k++);
			for (++k; location[k] == ' '; k++);
			for (; location[k] != '\0'; k++)
				value[vpointer++] = location[k];
			value[vpointer] = '\0';
			vpointer = 0;
			//input handling
			//
			if (isexpression(value))
			{
				sheet[x][y].flag = 1;
				while (value[vpointer] != '\0')
				{
					sheet[x][y].expression[vpointer] = value[vpointer++];
				}
				sheet[x][y].expression[vpointer] = value[vpointer];
			}
			else
			{
				sheet[x][y].flag = 0;

				sheet[x][y].value = 0;
				while (value[vpointer] != '\0')
					sheet[x][y].value = sheet[x][y].value * 10 + (1 + toint(value[vpointer++]));
			}
		}
		else if (strcmp(command, "PRINT") == 0)
		{
			for (int i = 0; location[i] != '\0'; i++)
			{
				location[i] = towupper(location[i]);
			}

			for (int x = 0; x < 10; x++)
			{
				for (int y = 0; y < 10; y++)
				{
					if (sheet[x][y].flag == 0)
						printf("%d,", sheet[x][y].value);
					else{
						int* intarray = (int *)malloc(sizeof(int) * 100);
						int* chararray = (int *)malloc(sizeof(int) * 100);
						int pointer = (int)malloc(sizeof(int));
						pointer = 0;
						int result = getinternal(location, sheet, chararray, intarray, pointer);
						printf("%d,", result);

					}
				}
				printf("\n");
			}


		}
		else if (strcmp(command, "EXPORT") == 0)
		{
			FILE *fp;

			isopen = 1;
			strcpy(filename, getfilename(location,0));
			fp = fopen(filename, "w");

			for (int i = 0; i < 10; i++)
			{
				for (int j = 0; j < 10; j++)
				{

					if (sheet[i][j].flag == 0)
						fprintf(fp, "%d,", sheet[i][j].value);

					else
					{
						fprintf(fp, "%s,", sheet[i][j].expression);
					}

				}
				fprintf(fp, "\n");
			}
			fclose(fp);
		}
		else if (strcmp(command, "SAVE") == 0)
		{
			if (!isopen)
				printf("no file open");
			else
			{
				FILE *fp;
				fp = fopen(filename, "w");

				for (int i = 0; i < 10; i++)
				{
					for (int j = 0; j < 10; j++)
					{

						if (sheet[i][j].flag == 0)
							fprintf(fp, "%d,", sheet[i][j].value);

						else
						{
							fprintf(fp, "%s,", sheet[i][j].expression);
						}

					}
					fprintf(fp, "\n");
				}
				fclose(fp);
			}
		}


		if (strcmp(command, "QUIT") == 0)
			return 0;
	}

}