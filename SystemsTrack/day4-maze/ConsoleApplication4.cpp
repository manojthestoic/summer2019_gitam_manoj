// ConsoleApplication4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
int solve(int ** maze, int m, int n, int endi, int endj, int curri, int currj, int **solution, int** visited);
void print(int **maze)
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			printf("%d", maze[i][j]);
		}
		printf("\n");
	}
}
int movedirection(int **maze, int m, int n, int endi, int endj, int curri, int currj, int direction, int** visited, int** solution)
{
	int directions[] = { 8, 6, 2, 4 };
	if (direction == 8)
		return solve(maze, m, n, endi, endj, curri - 1, currj, solution, visited);
	if (direction == 6)
		return	solve(maze, m, n, endi, endj, curri, currj + 1, solution, visited);
	if (direction == 2)
		return	solve(maze, m, n, endi, endj, curri+1, currj , solution, visited);
	if (direction == 4)
		return	solve(maze, m, n, endi, endj, curri, currj - 1, solution, visited);

}

int  movepossible(int ** maze, int m, int n, int curri, int currj, int direction, int ** visited, int **solution)
{

	if (curri - 1 >=0&&maze[curri -1][currj] != 0 && direction == 8 && visited[curri - 1][currj]!=1 && solution[curri - 1][currj]!=2)
		return 1;
	else if (currj + 1 <n  && maze[curri ][currj+1] != 0 && direction == 6 && visited[curri ][currj+1]!=1 && solution[curri][currj+1]!=2)
		return 1;
	else if (curri+ 1 <m && maze[curri+1][currj ] != 0 && direction == 2 && visited[curri+1][currj]!=1 && solution[curri +1][currj]!=2)
		return 1;
	else if (currj - 1 >=0&&maze[curri][currj - 1] != 0 && direction == 4 && visited[curri][currj - 1]!=1 && solution[curri][currj-1]!=2)
		return 1;
	return 0;
}

int solve(int ** maze, int m, int n, int endi, int endj, int curri, int currj, int **solution, int** visited)
{
	int directions[] = { 8, 6, 2, 4 };
	if (curri == endi &&currj == endj)
	{
		solution[endi][endj] = 5;
		print(solution);
		return 1;
	}
	for (int i = 0; i < 4; i++)
	{
		if (movepossible(maze, m, n, curri, currj, directions[i], visited, solution))
		{
			solution[curri][currj] = 2;
			if (1 == movedirection(maze, m, n, endi, endj, curri, currj, directions[i], visited, solution))
				return 1;
			solution[curri][currj] = 0;
		}

	}
	visited[curri][currj] = 1;
	return 0;
}



int main()
{
	int **maze = (int**)malloc(sizeof(int*)*10);
	for (int i = 0; i < 10; i++)
		maze[i] = (int*)malloc(sizeof(int) * 10);
	int **solution = (int**)malloc(sizeof(int*) * 10);
	for (int i = 0; i < 10; i++)
		solution[i] = (int*)malloc(sizeof(int) * 10);
	int **visited = (int**)malloc(sizeof(int*) * 10);
	for (int i = 0; i < 10; i++)
		visited[i] = (int*)malloc(sizeof(int) * 10);

	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)
			 maze[i][j]=1;
	maze[0][1] = 0;
	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)
			solution[i][j]=0;

	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)
			visited[i][j]=0;
	solve( maze,  10, 10, 9, 9,0, 0, solution, visited);
	return 0;
}

