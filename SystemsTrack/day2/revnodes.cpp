#include "node.h"

node* createlist(int *arr, int len)
{
	node *head = (node*)malloc(sizeof(node));
	head->next = NULL;
	head->value = arr[0];
	node* curr = head;

	for (int i = 1; i < len; i++)
	{
		node *newnode = (node*)malloc(sizeof(node));
		newnode->value = arr[i];
		newnode->next = NULL;
		curr->next = newnode;

		curr = curr->next;
	}
	return head;
}

node* listreversalrecursion(node *head)
{
	if (head->next == NULL)
		return head;
	node* nhead = listreversalrecursion(head);
	nhead->next = head;
	return nhead;
}