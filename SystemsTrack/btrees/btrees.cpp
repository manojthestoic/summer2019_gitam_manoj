// btrees.cpp : Defines the entry point for the console application.
//
#include<string.h>
#include "stdafx.h"
#include<stdlib.h>
#define nonleaftype 2
#define leafpagetype 1
struct leafpage
{
	char pagetype;
	char unused[4];
	int id[3];
	char name[3][5];
};
struct nonleafpage
{
	char pagetype;
	char unused[3];
	int children[4];
	int data[3];
};
char *remove_newline(char *str){
	int i = 0;
	while (str[i] != '\0'){
		if (str[i] == '\n'){
			str[i] = '\0';
			break;
		}
		i++;
	}
	return str;
}
int main()
{
	char mode;
	printf("read or write");
	scanf("%c", &mode);
	if (mode == 'w'){
		//write
		printf("write");
		leafpage sid[4];
		FILE *fi = fopen("input.txt", "r");
		char line[30];
		int i = 0, j = 0;
		sid[0].pagetype = '1';
		while (fgets(line, 30, fi))
		{
			char* comm = strchr(line, ',');
			*comm = '\0';
			comm++;
			strcpy(sid[i].name[j], remove_newline(comm));

			sid[i].id[j] = atoi(line);
			j++;
			if (j == 3)
			{
				i++;
				j = 0;
			}
		}
		for (int i = 0; i < 4; i++)
		{ 
			sid[i].pagetype = '1';
		}
			/*
		sid[0].pagetype = '1';
		sid[0].id[0] = 10;
		strcpy(sid[0].name[0], "Amar");
		sid[0].id[1] = 20;
		strcpy(sid[0].name[1], "Ana");
		sid[0].id[2] = 30;
		strcpy(sid[0].name[2], "Bala");

		sid[1].pagetype = '1';
		sid[1].id[0] = 40;
		strcpy(sid[1].name[0], "Bapu");
		sid[1].id[1] = 50;
		strcpy(sid[1].name[1], "Don");
		sid[1].id[2] = 60;
		strcpy(sid[1].name[2], "Egaa");

		sid[2].pagetype = '1';
		sid[2].id[0] = 70;
		strcpy(sid[2].name[0], "Fana");
		sid[2].id[1] = 80;
		strcpy(sid[2].name[1], "Guna");
		sid[2].id[2] = 90;
		strcpy(sid[2].name[2], "Hema");

		sid[3].pagetype = '1';
		sid[3].id[0] = 100;
		strcpy(sid[3].name[0], "Isac");
		sid[3].id[1] = 110;
		strcpy(sid[3].name[1], "Jai");
		sid[3].id[2] = 120;
		strcpy(sid[3].name[2], "Sara");
		*/
		FILE *fp = fopen("store.txt", "wb");
		for (int i = 0; i < 4; i++)
		{
			fwrite(&sid[i], sizeof(leafpage), 1, fp);
		}
		fclose(fp);
	}
	/*
	nonleafpage *head = (nonleafpage*)malloc(sizeof(nonleafpage));
	head->pagetype = leafpagetype;
	head->data[0] = 40;
	head->data[1] = 50;
	head->data[2] = 60;
	head->children[0] = 100;
	head->children[1] = 110;
	head->children[2] = 120;
	head->children[3] = 130;
	*///	FILE *fp = fopen("store.txt", "wb");
	//fwrite(head, sizeof(nonleafpage), 1, fp);
	//fclose(fp);
	else if (mode == 'r')
	{

		printf("read");
		leafpage read[4];
		nonleafpage reader;
		FILE* fp = fopen("store.txt", "rb+");
		//nonleafpage *read = (nonleafpage*)malloc(sizeof(nonleafpage));
		for (int i = 0; i < 4; i++)
			fread(&read[i], sizeof(leafpage), 1, fp);
		nonleafpage page;
		page.pagetype = '2';
		page.children[0] = 1;
		page.data[0] = read[0].id[0];
		for (int i = 1; i < 4; i++)
		{
			page.children[i] = i + 1;
			//page.data[i] = sid[i].id[0];
		}
		for (int i = 0; i < 3; i++)
		{
			page.data[i] = read[i].id[0];
		}
		FILE *fw = fopen("temp.txt", "wb");
		fwrite(read, sizeof(leafpage), 4, fw);
		fwrite(&page, sizeof(nonleafpage), 1, fw);
		printf("done");
	}
	return 0;
}
struct pageinfo
{
	char pagebuffer[sizeof(leafpage)];
	bool used;
	int lastused;
	int pageid;
};
void assignpage(pageinfo* info,int *used,int pageno)
{
	int slot = -1;
	int leastused = 999;
		for (int i = 0; i < 3; i++)
			if (info[i].used == 0)
				slot = i;
	if (slot==-1)
	{ 
		for (int i = 0; i < 3; i++)
		{
			if (leastused < info[i].lastused)
			{
				leastused = info[i].lastused;
				slot = i;
			}
		}
	}
	FILE *fp = fopen("temp.txt", "rb");
	fseek(fp, sizeof(leafpage) * pageno, SEEK_SET);
	fread(info[slot].pagebuffer, sizeof(leafpage), 1, fp);
	info[slot].lastused = *used++;
	info[slot].pageid = pageno;
	info[slot].used = 1;
}
int searchfornumber( pageinfo info,int q)
{

}
void search(int *quries,int num)
	{
		int used = 0;
		int req = -1;
		struct pageinfo info[3];
		FILE *fp = fopen("temp.txt","rb");
		assignpage(info, &used, 4);
		for (int i = 0; i < num; i++)
		{
			req = -1;
			for (int j = 0; i < 3; i++)
			{
				if (info[j].used == 1 && info[j].pagebuffer[0] == 2)
				{
					req = searchfornumber(info[j], quries[i]);
				}

			}
			if (req == -1)
			{
				assignpage(info, &used, 4);
				for (int j = 0; i < 3; i++)
				{
					if (info[j].used == 1 && info[j].pagebuffer[0] == 2)
					{
						req = searchfornumber(info[j], quries[i]);
					}

				}
			}



		}
	}

	*/