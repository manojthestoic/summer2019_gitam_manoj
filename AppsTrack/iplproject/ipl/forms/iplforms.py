from django import forms
from ipl.models import *


class Login(forms.Form):
    username= forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter username'})

    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'enter password'})

    )


class Signup(forms.Form):
    first_name = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter firstname'})

    )

    last_name = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter lastname'})

    )

    username = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter username'})

    )
    password = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'enter password'})

    )
    email= forms.EmailField(
        required=True,
        max_length=100,
        widget=forms.EmailInput(attrs={'class':'input','placeholder':'enter email'})
    )

class UserImage(forms.ModelForm):
    class Meta:
        model = Userimage
        exclude = ['user']
        widgets = {
            'url': forms.URLInput(attrs={'class': 'input', 'placeholder': 'enter URL'}),
        }