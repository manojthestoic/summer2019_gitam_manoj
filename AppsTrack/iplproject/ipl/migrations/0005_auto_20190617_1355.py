# Generated by Django 2.2.1 on 2019-06-17 08:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ipl', '0004_matches_player_of_match'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matches',
            name='dl_applied',
            field=models.CharField(blank=True, max_length=32),
        ),
        migrations.AlterField(
            model_name='matches',
            name='result',
            field=models.CharField(blank=True, max_length=32),
        ),
        migrations.AlterField(
            model_name='matches',
            name='toss_decision',
            field=models.CharField(max_length=32),
        ),
    ]
