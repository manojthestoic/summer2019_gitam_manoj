# Generated by Django 2.2.1 on 2019-06-18 06:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ipl', '0006_auto_20190617_1431'),
    ]

    operations = [
        migrations.CreateModel(
            name='Userimage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(default='https://akm-img-a-in.tosshub.com/indiatoday/images/story/201810/Virat_Kohli_10.jpeg?EI2FZcVodAjZU2s3aBMpAJ5rFHztxQ5h', max_length=250)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
