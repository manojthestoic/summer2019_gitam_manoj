# Generated by Django 2.2.1 on 2019-06-17 08:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ipl', '0003_matches_dl_applied'),
    ]

    operations = [
        migrations.AddField(
            model_name='matches',
            name='player_of_match',
            field=models.CharField(blank=True, max_length=64),
        ),
    ]
