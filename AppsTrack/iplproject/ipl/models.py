from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Matches(models.Model):
    matchid = models.IntegerField(unique=True)
    season = models.IntegerField()
    city = models.CharField(max_length=64, blank=True)
    date = models.DateField()
    team1 = models.CharField(max_length=64)
    team2 = models.CharField(max_length=64)
    tosswinner = models.CharField(max_length=64)
    toss_decision = models.CharField(max_length=32)
    result = models.CharField(max_length=32, blank=True)
    dl_applied = models.CharField(max_length=32, blank=True)
    winner = models.CharField(max_length=64, blank=True)
    win_by_runs = models.IntegerField()
    win_by_Wickets = models.IntegerField()
    player_of_match = models.CharField(max_length=64, blank=True)
    venue = models.CharField(max_length=64, blank=True)
    umpire1 = models.CharField(max_length=64, blank=True)
    umpire2 = models.CharField(max_length=64, blank=True)
    umpire3 = models.CharField(max_length=64, blank=True)


class Deliveries(models.Model):
    match = models.ForeignKey(Matches, on_delete=models.CASCADE, to_field="matchid")
    inning = models.IntegerField()
    battingteam = models.CharField(max_length=64)
    bowlingteam = models.CharField(max_length=64)
    over = models.IntegerField()
    ball = models.IntegerField()
    batsman = models.CharField(max_length=64)
    non_stricker = models.CharField(max_length=64)
    bowler = models.CharField(max_length=64)
    issuperover = models.IntegerField()
    wide_runs = models.IntegerField()
    bye_runs = models.IntegerField()
    legbye_runs = models.IntegerField()
    noball_runs = models.IntegerField()
    penaltyruns = models.IntegerField()
    batsman_runs = models.IntegerField()
    extra_run = models.IntegerField()
    totalruns_runs = models.IntegerField()
    player_dismissed = models.CharField(max_length=64, blank=True)
    dismissal_kind = models.CharField(max_length=64, blank=True)
    fielder = models.CharField(max_length=64, blank=True)

class Userimage(models.Model):
    url=models.URLField(max_length=250,default="https://akm-img-a-in.tosshub.com/indiatoday/images/story/201810/Virat_Kohli_10.jpeg?EI2FZcVodAjZU2s3aBMpAJ5rFHztxQ5h",blank=True)
    user=models.ForeignKey(User,on_delete=models.CASCADE)