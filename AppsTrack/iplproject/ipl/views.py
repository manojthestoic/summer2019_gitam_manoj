from django.shortcuts import render,redirect
from ipl.models import *
# Create your views here.
from django.views import View
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin

def Logout(request):
    logout(request)
    return redirect("season")


class LoginControler(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('season')
        from ipl.forms.iplforms import Login
        form1 = Login()
        context = {
            'form': form1,
        }
        return render(request, 'login.html', context)

    def post(self, request, *args, **kwargs):
        from ipl.forms.iplforms import Login
        form=Login(request.POST)
        if form.is_valid():
            user = authenticate(request,**form.cleaned_data)
        if user is not None:
            login(request, user)
            return redirect('season')
        else:
            from ipl.forms.iplforms import Login
            messages.error(request, 'invalid credentials')
            form1 = Login(request.POST)
            return render(request, 'login.html', context={'form': form1})


class SigninControler(View):
    def get(self, request, *args, **kwargs):
        from ipl.forms.iplforms import Signup,UserImage
        form1 = Signup()
        form2 = UserImage()
        context = {
            'form': form1,
            "image":form2
        }
        return render(request, 'signup.html', context)

    def post(self, request, *args, **kwargs):
        from ipl.forms.iplforms import Signup,UserImage
        from django.contrib.auth.models import User
        form1 = Signup(request.POST)
        form2=UserImage(request.POST)
        if form1.is_valid():
            user = User.objects.create_user(**form1.cleaned_data)
            user.save()
        if form2.is_valid():
            x=form2.save(commit=False)
            x.user=user
            x.save()
            if user is not None:
                login(request, user)
                return redirect('season')


class MatchView(LoginRequiredMixin,View):
    login_url = 'login'
    def get(self, request, *args, **kwargs):

        c = Matches.objects.get(**kwargs)
        if c.tosswinner ==c.team1:
            if c.toss_decision=='bat':
                firstbattingteam=c.team1
                secondbattingteam=c.team2
            else :
                secondbattingteam=c.team1
                firstbattingteam=c.team2
        else:
            if c.toss_decision=='bat':
                secondbattingteam=c.team1
                firstbattingteam=c.team2
            else :
                firstbattingteam=c.team1
                secondbattingteam = c.team2

        ball_by_ball = Deliveries.objects.filter(match_id=kwargs.get("matchid"))
        scores={c.team2:0,c.team1:0}
        extras = {c.team2: 0, c.team1: 0}
        list1 = []
        list2 = []
        list3 = []
        list4 = []
        runs=dict()
        wickets=dict()
        if c.result == 'normal':
            result = 1
            for ball in ball_by_ball:
                scores[ball.battingteam]+=ball.totalruns_runs
                if ball.batsman_runs>0:
                    if runs.get(ball.batsman):
                        runs[ball.batsman][0]+=ball.batsman_runs
                    else:
                        runs.update({ball.batsman: [ball.batsman_runs,ball.battingteam]})
                else:
                    extras[ball.bowlingteam]+=ball.totalruns_runs
                if ball.dismissal_kind !='':
                    ball.bighit="out"
                    if wickets.get(ball.bowler):
                        wickets[ball.bowler][0]+=1
                    else:
                        wickets.update({ball.bowler: [1,ball.bowlingteam]})
                elif ball.batsman_runs == 4:
                    ball.bighit= "four"
                elif ball.batsman_runs == 6:
                    ball.bighit= "six"
                else:
                    ball.bighit= False
                if ball.inning == 1:
                    list1.append(ball)
                else:
                    list2.append(ball)
        else:
            result = 0
            for ball in ball_by_ball:
                scores[ball.battingteam] += ball.totalruns_runs
                if ball.batsman_runs>0:
                    if runs.get(ball.batsman):
                        runs[ball.batsman][0]+=ball.batsman_runs
                    else:
                        runs.update({ball.batsman: [ball.batsman_runs,ball.battingteam]})
                else:
                    extras[ball.bowlingteam] += ball.totalruns_runs
                if ball.dismissal_kind!='':
                    ball.bighit="out"
                    if wickets.get(ball.bowler):
                        wickets[ball.bowler][0]+=1
                    else:
                        wickets.update({ball.bowler: [1,ball.bowlingteam]})

                elif ball.batsman_runs == 4:
                    ball.bighit= "four"
                elif ball.batsman_runs == 6:
                    ball.bighit= "six"
                else:
                    ball.bighit= False
                if ball.inning == 1:
                    list1.append(ball)
                elif ball.inning == 2:
                    list2.append(ball)
                elif ball.inning == 3:
                    list3.append(ball)
                else:
                    list4.append(ball)
        sorted_x = sorted(runs.items(), key=lambda kv: kv[1][0],reverse=True)
        sorted_y = sorted(wickets.items(), key=lambda kv: kv[1][0], reverse=True)
        wicketlist=[]

        diclist=[]
        limiter=0
        for x in sorted_x:
            if limiter==3:
                break
            diclist.append({'player':x[0],'runs':x[1][0],'team':x[1][1]})
            limiter+=1
        limiter=0
        for y in sorted_y:
            if limiter == 3:
                break
            wicketlist.append({'player': y[0], 'wickets': y[1][0],'team':y[1][1]})
            limiter += 1
        c.score1=scores
        url=""
        if  request.user.is_authenticated:
            url = Userimage.objects.values('url').get(user=request.user)


        context = {
            'match': c,
            'title': 'match',
            "list1": list1,
            "list2": list2,
            "list3": list3,
            "list4": list4,
            "b1":firstbattingteam,
            'b2':secondbattingteam,
            "ball_by_ball": ball_by_ball,
            "result": result,
            "runs":diclist,
            "wickets":wicketlist,
            "score1":scores[c.team1],
            "score2": scores[c.team2],
            "extra1": extras[c.team1],
            "extra2": extras[c.team2],
            "userimg":url
        }
        return render(request, 'matches.html', context)


class SeasonView(View):

    def get(self, request, *args, **kwargs):
        if kwargs:
            c = Matches.objects.filter(season=kwargs.get('year'))
            pages=[]
            page=[]
            pageno = 0

            url = ""
            if request.user.is_authenticated:
                url = Userimage.objects.values('url').get(user=request.user)
            for i, match in enumerate(c):
                page.append(match)
                if (i + 1) % 8 == 0:
                    pages.append({'page': page, 'pageno': pageno})
                    pageno += 1
                    page = []
                context = {
                    'matchlist': c,
                    'title': 'seasons',
                    'seasons': [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
                    'currentseason': kwargs.get('year'),
                    'pages': pages,
                    'userimg':url
                }

            return render(request, 'seasons.html', context)
        c = Matches.objects.filter(season=2019)
        pages = []
        page = []
        pageno=0

        url=""
        if  request.user.is_authenticated:
            url = Userimage.objects.values('url').get(user=request.user)
        for i, match in enumerate(c):
            page.append(match)
            if (i+1) % 8 == 0:
                pages.append({'page':page,'pageno':pageno})
                pageno+=1
                page = []
        context = {
            'matchlist': c,
            'title': 'seasons',
            'seasons': [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
            'currentseason': 2019,
            'pages':pages,
            'userimg':url
        }
        return render(request, 'seasons.html', context)


class PointsTable(View):
    def get(self, request, *args, **kwargs):
        if kwargs:
            teams1=Matches.objects.values('team1').filter(season=kwargs.get('year')).distinct()
            c = Matches.objects.values('team1','team2','winner').filter(season=kwargs.get('year'))
            points=dict()
            for team in teams1:
                points.update({team['team1']:0})
            for match in c:
                x=match["winner"]
                if points.get(match["winner"]):
                    points[match['winner']] += 2
                else:
                    points[match['team1']]+=1
                    points[match['team2']]+=1
            table=[]
            for team,score in points.items():
                table.append({'team':team,'score':score})

            url = ""
            if request.user.is_authenticated:
                url = Userimage.objects.values('url').get(user=request.user)
            context = {
                    'matchlist': c,
                    'title': 'pointlist',
                    'seasons': [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
                    'currentseason': kwargs.get('year'),
                    'userimg':url,
                    'table':table
                }

            return render(request, 'points.html', context)
