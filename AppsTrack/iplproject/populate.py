# import MySQLdb
# db = MySQLdb.connect(host="localhost",
#                      user="root",
#                      passwd="m@ngas0ul",
#                      )
# cur = db.cursor()
# cur.execute("CREATE DATABASE if not exists ipl")

#
import csv
import os
import django
from django.core.exceptions import ValidationError
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'iplmocktest.settings')
django.setup()

from ipl.models import*
def load_data_into_table():
    fields = []
    with open("deliveries.csv", 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        fields = csvreader.__next__()

        for i,team in enumerate(csvreader):
            if i<58878:
                continue
            if len(team)!=0:
                m = Deliveries(match = Matches.objects.get(matchid=team[0]),inning = team[1],battingteam=team[2],bowlingteam=team[3],over=team[4],ball=team[5],batsman=team[6],non_stricker=team[7],bowler=team[8],issuperover=team[9],wide_runs=team[10],bye_runs=team[11],legbye_runs=team[12],noball_runs=team[13],penaltyruns=team[14],batsman_runs=team[15],extra_run=team[16],totalruns_runs=team[17],player_dismissed=team[18],dismissal_kind=team[19],fielder=team[20])
                m.save()
            else:
                continue



if __name__ == '__main__':
    load_data_into_table()