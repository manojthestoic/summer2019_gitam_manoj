"""iplmocktest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from ipl.views import SeasonView,MatchView,Logout,LoginControler,SigninControler,PointsTable
urlpatterns = [
    path('admin/', admin.site.urls),
    path('logout/',Logout,name='logout'),
    path('login/', LoginControler.as_view(),name="login"),
    path('signin/', SigninControler.as_view(),name="signin"),
    path('season/',SeasonView.as_view(),name="season"),
    path('season/<int:year>',SeasonView.as_view(),name="seasonyear"),
    path('match/<int:matchid>',MatchView.as_view(),name='match'),
    path('points/<int:year>', PointsTable.as_view(), name="pointyear")

]
