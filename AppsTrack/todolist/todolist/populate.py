import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'todolist.settings')
django.setup()
from todo.models import *


def populate():
    for i in range(0, 5):
        string="list" + str(i)
        c = Todolist(name=string)
        c.save()
        for j in range(0, 5):
            string="todo list" + str(i) + ' item' + str(j)
            item = Todoitem(description=string)
            item.list = c
            item.save()
# populate()