from django.shortcuts import render
from django.urls import resolve
from django.views import View
from todo.models import Todolist, Todoitem
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib import messages
from django.shortcuts import redirect
from django.http import HttpResponse,HttpResponseForbidden


# Create your views here.
def Logout(request):
    logout(request)
    return redirect("login")


class LoginControler(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('lists')
        from todo.forms.forms import Login
        form1 = Login()
        context = {
            'form': form1,
        }
        return render(request, 'login.html', context)

    def post(self, request, *args, **kwargs):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('lists')
        else:
            from todo.forms.forms import Login
            messages.error(request, 'invalid credentials')
            form1 = Login(request.POST)
            return render(request, 'login.html', context={'form': form1})


class SigninControler(View):
    def get(self, request, *args, **kwargs):
        from todo.forms.forms import Signup
        form1 = Signup()
        context = {
            'form': form1,
        }
        return render(request, 'signin.html', context)

    def post(self, request, *args, **kwargs):
        from todo.forms.forms import Signup
        from django.contrib.auth.models import User
        form1 = Signup(request.POST)
        if form1.is_valid():
            user = User.objects.create_user(username=form1.cleaned_data["username"],
                                            password=form1.cleaned_data["password"],
                                            first_name=form1.cleaned_data["Firstname"],
                                            last_name=form1.cleaned_data["lastname"])
            user.save()
            if user is not None:
                login(request, user)
                return redirect('lists')


def firstview(request):
    return HttpResponse("<body><h1>firstview</h1></body>")


class ListoflistView(LoginRequiredMixin, PermissionRequiredMixin, View):
    login_url = 'login'
    permission_required = ('todo.view_todolist')

    def get(self, request, *args, **kwargs):
        if kwargs:
            specificlist = Todolist.objects.get(**kwargs)
            listitem = list(specificlist.todoitem_set.all())
            context = {
                'item_list': listitem,
                'title': specificlist,
                'permissions': request.user.get_all_permissions()
            }
            return render(request, 'list.html', context)
        t = request.user.todolist_set.all()
        context = {
            'todo_lists': t,
            'title': 'todo list | mentor app',
            'permissions': request.user.get_all_permissions()
        }
        return render(request, 'index.html', context)

    def post(self, request, *args, **kwargs):
        x = request.POST.items()
        specificlist = Todolist.objects.get(**kwargs)
        listitem = list(specificlist.todoitem_set.all())
        for item in listitem:
            item.completed = False
            item.save()
        for i in x:
            if i[0] == "csrfmiddlewaretoken":
                continue
            item = Todoitem.objects.get(id=i[0])
            item.completed = True
            item.save()

        specificlist = Todolist.objects.get(**kwargs)
        listitem = list(specificlist.todoitem_set.all())
        context = {
            'item_list': listitem,
            'title': specificlist,
            'permissions': request.user.get_all_permissions()
        }
        return render(request, 'list.html', context)
        pass


class AddList(View,LoginRequiredMixin):
    login_url = 'login'
    def get(self, request, *args, **kwargs):
        from todo.forms.forms import AddListForm

        form1 = AddListForm()
        title = "add list"
        errors=''
        if resolve(request.path_info).url_name == 'deletelist':
            Todolist.objects.get(id=kwargs["id"]).delete()
            return redirect('lists')
        if kwargs.get("id"):
            list = Todolist.objects.get(id=kwargs["id"])
            form1 = AddList(instance=list)
            title = "edit list name"
            # else:
            #     errors='no  edit permissions'
        context = {
            'form': form1,
            'title': title,
            'errors': errors
        }
        if 'todo.edit_todolist' in request.user.get_all_permissions() or 'todo.add_todolist' in request.user.get_all_permissions():

            return render(request, 'add.html', context)
        else:
            return HttpResponseForbidden('no proper permissions')

    def post(self, request, **kwargs):
        from todo.forms.forms import AddListForm
        form1 = AddListForm(request.POST)
        if resolve(request.path_info).url_name == 'editlist':
            List = Todolist.objects.get(id=kwargs["id"])
            form1 = AddListForm(request.POST, instance=List)
        if form1.is_valid():
            x=form1.save(commit=False)
            x.user=request.user
            x.save()
            return redirect('lists')


class AddItem(View, LoginRequiredMixin):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        from todo.forms.forms import Additem

        form1 = Additem()
        title = "add item"
        errors=''
        if resolve(request.path_info).url_name == 'deleteitem':
            Todoitem.objects.get(id=kwargs["iid"]).delete()
            return redirect('listsid', id=kwargs["id"])
        if kwargs.get("iid"):
            item = Todoitem.objects.get(id=kwargs["iid"])
            form1 = Additem(instance=item)
            title = "edit item"
            # else:
            #     errors='no  edit permissions'
        context = {
            'form': form1,
            'title': title,
            'errors': errors
        }
        if 'todo.edit_todoitem' in request.user.get_all_permissions() or 'todo.add_todoitem' in request.user.get_all_permissions():

            return render(request, 'add.html', context)
        else:
            return HttpResponseForbidden('no proper permissions')

    def post(self, request, **kwargs):
        from todo.forms.forms import Additem
        form1 = Additem(request.POST)
        if resolve(request.path_info).url_name == 'edititem':
            item = Todoitem.objects.get(id=kwargs["iid"])
            form1 = Additem(request.POST, instance=item)
        if form1.is_valid():
            x=form1.save(commit=False)
            x.listb = Todolist.objects.get(id=kwargs['id'])
            x.save()
            return redirect('listsid', id=kwargs['id'])
