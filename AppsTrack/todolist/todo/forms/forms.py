from django import forms
from todo.models import Todoitem,Todolist
class Login(forms.Form):
    username= forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter username'})

    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'enter password'})

    )


class Signup(forms.Form):
    Firstname = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter firstname'})

    )

    lastname = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter lastname'})

    )

    username = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter username'})

    )
    password = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'enter password'})

    )

class Additem(forms.ModelForm):
    class Meta:
        model = Todoitem
        exclude = ['id','listb']
        widgets = {
            'description' : forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter name or description'}),
        'due_date': forms.DateTimeInput(attrs={ 'placeholder': 'enter date'}),

        }

class AddListForm(forms.ModelForm):
    class Meta:
        model = Todolist
        exclude = ['id','user']
        widgets={
            'name':forms.TextInput(attrs={'class':'input','placeholder':'enter list name'})
        }