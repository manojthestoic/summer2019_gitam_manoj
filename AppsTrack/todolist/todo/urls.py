from django.urls import include, path
# from todo.views import ListoflistView
from todo.views import *
# from todo.views import LoginControler,SigninControler,Logout
urlpatterns = [
    path('lists/', ListoflistView.as_view(), name='lists'),
    path('login/', LoginControler.as_view(), name='login'),
    path('signin/', SigninControler.as_view(), name='signin'),
    path('logout/', Logout, name='logout'),
    path('lists/<int:id>', ListoflistView.as_view(), name='listsid'),
    path('lists/<int:id>/add', AddItem.as_view(), name='additem'),
    path('lists/<int:id>/delete/<int:iid>', AddItem.as_view(), name='deleteitem'),
    path('lists/<int:id>/edit/<int:iid>', AddItem.as_view(), name='edititem'),
    path('lists/add', AddList.as_view(), name='addlist'),
    path('lists/<int:id>/delete', AddList.as_view(), name='deletelist'),
    path('lists/<int:id>/edit', AddList.as_view(), name='editlist'),
    path('test',firstview)
]
