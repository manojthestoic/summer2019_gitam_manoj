from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Todolist(models.Model):
    name = models.CharField(max_length=64)
    created = models.DateTimeField(auto_now_add=True)
    user=models.ForeignKey(User, on_delete=models.CASCADE)


class Todoitem(models.Model):
    description = models.CharField(max_length=1000)
    due_date = models.DateTimeField(blank=True, null=True)
    completed = models.BooleanField(default=False)
    listb = models.ForeignKey(Todolist, on_delete=models.CASCADE)
