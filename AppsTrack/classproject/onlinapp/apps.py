from django.apps import AppConfig


class OnlinappConfig(AppConfig):
    name = 'onlinapp'
