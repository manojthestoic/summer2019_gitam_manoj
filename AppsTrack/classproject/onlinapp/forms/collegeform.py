from django import forms
from onlinapp.models import *


class Addcollege(forms.ModelForm):
    class Meta:
        model = College
        exclude = ['id']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter name'}),
            'location': forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter location'}),
            'acronym': forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter acronym'}),
            'contact': forms.EmailInput(attrs={'class': 'input', 'placeholder': 'enter email'})
        }


class Addstudent(forms.ModelForm):
    class Meta:
        model = Student
        exclude = ["id", "dob", "college"]
        widgets = {
            'name': forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter name'}),
            'email': forms.EmailInput(attrs={'class': 'input', 'placeholder': 'enter email'}),
            'db_folder': forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter dbfolder'}),
            "dropped_out": forms.CheckboxInput()
        }


class Addscores(forms.ModelForm):
    class Meta:
        model = MockTest1
        exclude = ["id", "student", "total"]
        widgets = {
            'problem1': forms.NumberInput(attrs={'class': 'input', 'placeholder': 'enter problem1'}),
            'problem2': forms.NumberInput(attrs={'class': 'input', 'placeholder': 'enter problem2'}),
            'problem3': forms.NumberInput(attrs={'class': 'input', 'placeholder': 'enter problem3'}),
            'problem4': forms.NumberInput(attrs={'class': 'input', 'placeholder': 'enter problem4'})
        }

class Login(forms.Form):
    username= forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter username'})

    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'enter password'})

    )


class Signup(forms.Form):
    Firstname = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter firstname'})

    )

    lastname = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter lastname'})

    )

    username = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'enter username'})

    )
    password = forms.CharField(
        required=True,
        max_length=75,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'enter password'})

    )