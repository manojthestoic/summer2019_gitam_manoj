from django.urls import include, path  # For django versions from 2.0 and up
from onlinapp.views import CollegeView
from onlinapp.views import Addcollegeview
from onlinapp.views import Addstudentview
from onlinapp.views import LoginControler
from onlinapp.views import SigninControler
from onlinapp.views import Logout
from onlinapp.views import rest_college,RestStudent
urlpatterns = [
    # path('getcolleges/', views.get_colleges),
    # path('getcollege/', views.get_college),
    # path('student_of_college/<int:id>/', views.student_of_college,name="studentlist")


    path('login/', LoginControler.as_view(), name='login'),
    path('signin/', SigninControler.as_view(), name='signin'),
    path('logout/', Logout, name='logout'),


    path('getcolleges/', CollegeView.as_view(), name='getcolleges1'),
    path('getcolleges/<int:id>', CollegeView.as_view(), name='getcolleges'),
    path('getcolleges/<str:acronym>', CollegeView.as_view(), name='getcollegesacr'),
    path('getcollege/add/', Addcollegeview.as_view(), name='addcollege'),
    path('getcolleges/<int:pk>/edit/', Addcollegeview.as_view(), name='editcollege'),
    path('getcolleges/<int:pk>/delete/', Addcollegeview.as_view(), name='deletecollege'),
    path('getcolleges/<int:pk>/addstudent', Addstudentview.as_view(), name='addstudent'),
    path('getcolleges/<int:pk>/editstudent/<int:student_id>', Addstudentview.as_view(), name='editstudent'),
    path('getcolleges/<int:pk>/deletestudent/<int:student_id>', Addstudentview.as_view(), name='deletestudent'),

    path('api/v1/getcolleges/', rest_college,name='apicollege'),
    path('api/v1/getcolleges/<int:id>/', rest_college,name='apicolleged'),

    path('api/v1/getcolleges/<int:id>/getstudents', RestStudent.as_view(),name='apistd'),
    path('api/v1/getcolleges/<int:id>/getstudents/<int:stuid>', RestStudent.as_view(),name='apistd')
]

from rest_framework.authtoken import views
urlpatterns += [
    path('api-token-auth/', views.obtain_auth_token)
]