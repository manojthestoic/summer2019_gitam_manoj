from django.contrib import admin
from onlinapp.models import *
# Register your models here.
admin.site.register(College)
admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(MockTest1)