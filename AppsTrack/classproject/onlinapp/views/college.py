from django.views import View
from onlinapp.models import *
from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import resolve
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib import messages


def Logout(request):
    logout(request)
    return redirect("login")


class LoginControler(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('getcolleges1')
        from onlinapp.forms import Login
        form1 = Login()
        context = {
            'form': form1,
        }
        return render(request, 'onlinapp/login.html', context)

    def post(self, request, *args, **kwargs):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('getcolleges1')
        else:
            from onlinapp.forms import Login
            messages.error(request, 'invalid credentials')
            form1 = Login(request.POST)
            return render(request, 'onlinapp/login.html', context={'form': form1})


class SigninControler(View):
    def get(self, request, *args, **kwargs):
        from onlinapp.forms import Signup
        form1 = Signup()
        context = {
            'form': form1,
        }
        return render(request, 'onlinapp/signin.html', context)

    def post(self, request, *args, **kwargs):
        from onlinapp.forms import Signup
        from django.contrib.auth.models import User
        form1 = Signup(request.POST)
        if form1.is_valid():
            user = User.objects.create_user(username=form1.cleaned_data["username"],
                                            password=form1.cleaned_data["password"],
                                            first_name=form1.cleaned_data["Firstname"],
                                            last_name=form1.cleaned_data["lastname"])
            user.save()
            if user is not None:
                login(request, user)
                return redirect('getcolleges1')


class CollegeView(LoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request, *args, **kwargs):
        if kwargs:
            c = College.objects.get(**kwargs)
            students = list(c.student_set.order_by("-mocktest1__total"))
            # students = Student.objects.values('name', 'email', 'db_folder', 'mocktest1__total').filter(college_id=c[0].id)

            context = {
                'student_list': students,
                'title': c,
                'permissions': request.user.get_all_permissions()
            }
            return render(request, 'onlinapp/studentofcollege.html', context)
        c = College.objects.all()
        # c = College.objects.values('name', 'acronym', "id")
        context = {
            'college_list': c,
            'title': 'all colleges | mentor app',
            'permissions': request.user.get_all_permissions()
        }
        return render(request, 'onlinapp/index.html', context)


class Addstudentview(LoginRequiredMixin,PermissionRequiredMixin, View):
    login_url = '/login/'
    permission_required = ('onlinapp.add_student','onlinapp.change_student','onlinapp.delete_student',)
    def get(self, request, *args, **kwargs):
        from onlinapp.forms import Addscores, Addstudent
        form1 = Addstudent()
        form2 = Addscores()
        if resolve(request.path_info).url_name == 'deletestudent':
            Student.objects.get(id=kwargs.get("student_id")).delete()
            return redirect('getcolleges', id=kwargs.get("pk"))
        if kwargs.get("student_id"):
            s = Student.objects.get(id=kwargs.get("student_id"))

            form1 = Addstudent(instance=s)
            form2 = Addscores(instance=s.mocktest1)

            # form1 = Addcollege(instance=coll)
        context = {
            'form1': form1,
            'form2': form2
        }
        return render(request, 'onlinapp/addstudent.html', context)

    def post(self, request, *args, **kwargs):
        from onlinapp.forms import Addscores, Addstudent
        form1 = Addstudent(request.POST)
        form2 = Addscores(request.POST)

        if resolve(request.path_info).url_name == 'editstudent':
            if form1.is_valid() and form2.is_valid():
                s = Student.objects.get(id=kwargs.get("student_id"))
                form1 = Addstudent(request.POST, instance=s)
                form2 = Addscores(request.POST, instance=s.mocktest1)
                m1 = form1.save(commit=False)
                m2 = form2.save(commit=False)
                m1.college = College.objects.get(id=kwargs.get('pk'))
                m1.save()
                m2.student = m1
                m2.total = m2.problem1 + m2.problem2 + m2.problem3 + m2.problem4
                m2.save()
                return redirect('getcolleges', id=kwargs.get('pk'))
            else:
                s = Student.objects.get(id=kwargs.get("student_id"))

                form1 = Addstudent(instance=s)
                form2 = Addscores(instance=s.mocktest1)

                # form1 = Addcollege(instance=coll)
            context = {
                'form1': form1,
                'form2': form2
            }
            return render(request, 'onlinapp/addstudent.html', context)

        m1 = form1.save(commit=False)
        m2 = form2.save(commit=False)
        m1.college = College.objects.get(**kwargs)
        m1.save()
        m2.student = m1
        m2.total = m2.problem1 + m2.problem2 + m2.problem3 + m2.problem4
        m2.save()
        return redirect('getcolleges', id=kwargs.get('pk'))

        pass


class Addcollegeview(LoginRequiredMixin,PermissionRequiredMixin, View):
    login_url = 'login'
    permission_required = ('onlinapp.add_college', 'onlinapp.change_college', 'onlinapp.delete_college',)
    def get(self, request, *args, **kwargs):
        from onlinapp.forms import Addcollege
        form1 = Addcollege()
        if resolve(request.path_info).url_name == 'deletestudent':
            Student.objects.get(**kwargs).delete()
            return redirect('getcolleges1')
        if kwargs:
            coll = College.objects.get(**kwargs)
            form1 = Addcollege(instance=coll)
        context = {
            'form': form1,
        }
        return render(request, 'onlinapp/addcollege.html', context)

    def post(self, request, *args, **kwargs):
        from onlinapp.forms import Addcollege
        form = Addcollege(request.POST)
        if resolve(request.path_info).url_name == 'editcollege':
            coll = College.objects.get(**kwargs)
            form = Addcollege(request.POST, instance=coll)
        if resolve(request.path_info).url_name == 'deletecollege':
            College.objects.get(**kwargs).delete()
            return redirect('getcolleges1')
        if form.is_valid():
            form.save()
            return redirect('getcolleges1')
