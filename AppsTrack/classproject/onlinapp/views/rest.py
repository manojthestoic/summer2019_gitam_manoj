from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from onlinapp.serializers import CollegeSerializer, StudentSerializer, StudentdetailSerializer
from onlinapp.models import *
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes,authentication_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
@authentication_classes((SessionAuthentication, TokenAuthentication,BasicAuthentication))
@permission_classes((IsAuthenticated,))
def rest_college(request, *args, **kwargs):
    if request.method == 'GET':
        if kwargs:
            college = get_object_or_404(College, **kwargs)
            serializer = CollegeSerializer(college)
            return Response(serializer.data)
        colleges = College.objects.all()
        serializer = CollegeSerializer(colleges, many=True)
        return Response(serializer.data)
    if request.method == 'POST':
        serializer = CollegeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)
    if request.method == 'DELETE':
        college = get_object_or_404(College, **kwargs)
        college.delete()
        return Response(status=204)

    if request.method == 'PUT':
        college = get_object_or_404(College, **kwargs)
        serializer = CollegeSerializer(college, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


class RestStudent(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        # college = get_object_or_404(College, id=kwargs['id'])
        if kwargs.get("stuid"):
            serializer = StudentdetailSerializer(Student.objects.get(id=kwargs['stuid']))
        else:
            serializer = StudentSerializer(Student.objects.filter(college=kwargs['id']), many=True)
        return Response(serializer.data)

    def post(self, request, **kwargs):
        serializer = StudentdetailSerializer(data=request.data, context={**kwargs})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def put(self, request, **kwargs):
        std = Student.objects.get(id=kwargs["stuid"])
        serializer = StudentdetailSerializer(instance=std, data=request.data, context={**kwargs})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def delete(self, request, **kwargs):
        Student.objects.get(id=kwargs['stuid']).delete()
        return Response(status=204)
